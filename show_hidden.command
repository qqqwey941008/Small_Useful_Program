#!/bin/bash

read -p "Do you want to show your hidden file?" answer
case ${answer:0:1} in
	y|Y ) defaults write com.apple.finder AppleShowAllFiles YES; echo "Show hidden file, please restart finder (alt)"
	;;
    * ) defaults write com.apple.finder AppleShowAllFiles NO; echo "Hide hidden file, please restart finder (alt)"
	;;
esac

# if [ "$1" = "NO" ]; then 
# 	defaults write com.apple.finder AppleShowAllFiles $1
# 	echo "Hide hidden file, please restart finder (alt)"
# elif [ "$1" = "YES" ]; then
# 	defaults write com.apple.finder AppleShowAllFiles $1
# 	echo "Show hidden file, please restart finder (alt)"
# else
# 	echo "usage: ./show_hidden.sh YES/NO"
# fi